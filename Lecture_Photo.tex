\documentclass[10pt,a5paper]{beamer}

% packages
\usepackage{xcolor}
\usepackage[utf8]{inputenc}
\usepackage{graphicx}
\usepackage[ngerman]{babel}
\usepackage[T1]{fontenc}
\usepackage{lmodern}

\usepackage{multicol}
\usepackage{nameref}
\usepackage{subfigure} % Multiple gaphics in one figure
\usepackage{diagbox}

% package configuration
\DeclareGraphicsExtensions{.pdf,.png,.jpg}
\beamertemplatenavigationsymbolsempty
\setbeamertemplate{footline}[frame number]

% style
\usetheme{default}
\usecolortheme{default}

% title page information
\author{Thilo Aschmutat,\\ Malte Flender}
\institute[FH Bielefeld]{Campus Minden\\ FH Bielefeld}
\title[Digitalphotographie]{Einführung in die Technik der Digitalphotographie}
\subject{Vortrag zur Technik der Digitalphotographie}
\date{22.08.2016}
\keywords{Digitalphotographie, Technik, Summer School 2016}

\newcommand{\button}[1]{
	\vfill
	\hfill
	\beamerbutton{#1}
}

% Macro for centering extreme wide tables/figures
\makeatletter
\newcommand*{\Centerfloat}{%
  \parindent \z@
  \leftskip \z@ \@plus 1fil \@minus \textwidth
  \rightskip\leftskip
  \parfillskip \z@skip}
\makeatother

% A text with quotation marks
% @par1: The text you want to quote
% »text«
\newcommand*{\QuoteM}[1]{\frqq #1\flqq}

\setbeamertemplate{section in toc}[sections numbered]
\setbeamertemplate{subsection in toc}[subsections numbered]

\setcounter{tocdepth}{4}

\makeatletter
\newcommand*{\currentname}{\@currentlabelname}
\makeatother

% Brake long url in cite
\def\UrlBreaks{\do\/\do-}

\AtBeginSubsection[]
{
  \begin{frame}<beamer>{Inhaltsverzeichnis}
   \begin{multicols}{2}
     \tableofcontents[currentsection]
   \end{multicols}
  \end{frame}
}

\begin{document}

\frame{
	\titlepage
}

\frame{
	\frametitle{Inhaltsverzeichnis}
	\begin{multicols}{2}
	\tableofcontents
	\end{multicols}
}
\section{Die Ausrüstung}

\subsection{Objektiv}

\subsubsection{Brennweite}
\begin{frame}{\currentname}
Die \textbf{Brennweite} (in mm) gibt den \textbf{Abstand} zwischen dem 
optischen Zentrum des \textbf{Objektives} und der \textbf{Sensorebene} an. 
Das optischen Zentrum kann auch außerhalb des Objektives liegen.

\bigskip
Die Brennweite und die Sensorgröße bestimmen den Bildausschnitt.

\bigskip
Einige Brennweiten, bezogen auf Kleinbildformat:
\begin{itemize}
\item 5--10 mm extrem Weitwinkel Fisheye
\item 10--35 mm extrem Weitwinkel
\item 35--40 mm Weitwinkel
\item 50 mm Normal
\item 50--200 mm Tele
\item 200--2000 mm Supertele
\end{itemize}

\end{frame}

\begin{frame}{\currentname}{Beispiele}

\begin{figure}[htb]
	\subfigure[24 mm]{%
		\includegraphics[scale=0.3]
		{Breinnweite/24mm.jpg}
		\label{subfig:focallength24}
	}
	\subfigure[50 mm]{%
		\includegraphics[scale=0.3]
		{Breinnweite/50mm.jpg}
		\label{subfig:focallength50}
	}
	\subfigure[100 mm]{%
		\includegraphics[scale=0.3]
		{Breinnweite/100mm.jpg}
		\label{subfig:focallength100}
	}
	\subfigure[400 mm]{%
		\includegraphics[scale=0.3]
		{Breinnweite/400mm.jpg}
		\label{subfig:focallength400}
	}
	%\caption{Auswirkungen der Brennweite auf die Bildgestaltung}
	\label{fig:focallength}
\end{figure}

\begin{footnotesize}
Alle Bilder sind mit einer Canon EOS 5D Mark II bei ISO 100 und Blende 8 
Aufgenommen.
\end{footnotesize}

\end{frame}

\subsubsection{Blende}
\begin{frame}{\currentname}
Die \textbf{Blendenzahl} gibt das \textbf{Verhältnis} aus \textbf{Brennweite} 
und \textbf{Öffnungsdurchmesser der Objektivblende} an, z.B. 100 mm 
Brennweite und 50 mm Blendendurchmesser ergeben eine Blende von $\frac{1}
{2,0}$ oder umgangssprachlich \QuoteM{Blende 2}.

\bigskip
Die Blende wird zu Steuerung der \textbf{Schärfentiefe} benutzt. Wobei man 
beachten muss, dass die Sensorgröße und der Objektabstand auch einen Einfluss 
auf die Schärfentiefe haben.

\bigskip
Dabei ist zu beachten, das je kleiner die \textbf{Blendenöffnung} ist je 
größer wird die durch die \textbf{Beugung} verursachte \textbf{Unschärfe}, 
dies gilt besonders in der Makro-Photographie.

\end{frame}

\begin{frame}{\currentname}{Beispiele}

\begin{figure}[htb]
	\subfigure[Blende: 2,8 Belichtung: $\frac{1}{6}$ s]{%
		\includegraphics[scale=0.3]
		{Blende/IMG_2353.JPG}
		\label{subfig:app2.8}
	}
	\subfigure[Blende: 4,0 Belichtung: 0,3 s]{%
		\includegraphics[scale=0.3]
		{Blende/IMG_2355.JPG}
		\label{subfig:app4.0}
	}
	\subfigure[Blende: 5,6 Belichtung: 0,6 s]{%
		\includegraphics[scale=0.3]
		{Blende/IMG_2356.JPG}
		\label{subfig:app5.6}
	}
	\subfigure[Blende: 8,0 Belichtung: 1,3 s]{%
		\includegraphics[scale=0.3]
		{Blende/IMG_2357.JPG}
		\label{subfig:app8.0}
	}
	%\caption{Auswirkungen der Blende auf die Bildgestaltung}
	\label{fig:appreture}
\end{figure}

\begin{footnotesize}
Alle Bilder sind mit einer Canon EOS 5D Mark II bei ISO 100 mit dem
Canon EF 100mm f/2.8L Macro IS USM Aufgenommen.
\end{footnotesize}

\end{frame}

\subsubsection{Optische Bildstabilisierung}
\begin{frame}{\currentname}

Als \textbf{Faustregel} für Freihandaufnahmen kann der\textbf{ Kehrwert der
Brennweite als maximale Belichtungszeit} benutzt werden, z.B. ergibt eine 
Brennweite von 150 mm eine maximale Belichtungszeit von $\frac{1}{150}$ s, 
unterhalb dieses Wertes sollte ein \textbf{Stativ} benutzt werden.

\bigskip
Um Verwacklungsunschärfe zu vermeiden gibt es bei vielen modernen Objektiven 
die Möglichkeit der \textbf{optischen Bildstabilisierung}. Dazu werden von 
Sensoren gemessene Schwingungen invertiert auf bewegliche Linsen übertragen.

\bigskip
Wird ein \textbf{Stativ} benutzt, sollte die \textbf{optische 
Bildstabilisierung ausgeschaltet} werden, da es sonnst möglicherweise zu 
Schärfeverlusten kommt.

\end{frame}

\subsection{Digitalkamera}

\subsubsection{Belichtungszeit}
\begin{frame}{\currentname}
Die \textbf{Zeit}, in der der \textbf{Sensor nicht} vom Verschluss 
\textbf{verdeckt} wird und belichtet wird ist die \textbf{Belichtungszeit}. 

\bigskip
Je \textbf{länger} ein Bild \textbf{belichtet} wird je mehr Licht trifft den 
Sensor und je \textbf{heller} wird das Bild, jedoch kann das Bild 
\textbf{verwackelt} werden.

\bigskip
Umgekehrt werden \textbf{kurz belichtete} Bilder \textbf{weniger verwackelt} 
und \textbf{dunkler}.
\end{frame}

\begin{frame}{\currentname}{Beispiel}
\begin{figure}[htb] \setcounter{subfigure}{0}
	\subfigure[Blende: 32,0 Belichtung: $\frac{1}{15}$ s]{%
		\includegraphics[scale=0.3]
		{Zeit/IMG_2386.jpg}
		\label{subfig:time15}
	}
	\subfigure[Blende: 29,0 Belichtung: $\frac{1}{50}$ s]{%
		\includegraphics[scale=0.3]
		{Zeit/IMG_2384.jpg}
		\label{subfig:time50}
	}
	\subfigure[Blende: 8,0 Belichtung: $\frac{1}{500}$ s]{%
		\includegraphics[scale=0.3]
		{Zeit/IMG_2381.jpg}
		\label{subfig:time500}
	}
	\subfigure[Blende: 5,6 Belichtung: $\frac{1}{1000}$ s]{%
		\includegraphics[scale=0.3]
		{Zeit/IMG_2380.jpg}
		\label{subfig:time1000}
	}
	%\caption{Auswirkungen der Belichtungszeit auf die Bildgestaltung}
	\label{fig:time}
\end{figure}
\begin{footnotesize}
Alle Bilder sind mit einer Canon EOS 5D Mark II bei ISO 100 mit dem Canon EF 
100mm f/2.8L Macro IS USM Aufgenommen.
\end{footnotesize}
\end{frame}
\subsubsection{Empfindlichkeit}
\begin{frame}{\currentname}
Die vom \textbf{Bildsensor} gemessenen elektrischen \textbf{Signale} werden 
\textbf{verstärkt}.

\medskip
Die \textbf{Empfindlichkeit} (Definiert in der ISO 5800) gibt an \textbf{wie 
stark} die \textbf{Verstärkung} ist.

\medskip
Eine \textbf{Verdoppelung} des ISO Wertes v\textbf{erdoppelt die Sensor-
Empfindlichkeit}.

\medskip
So kann die \textbf{Belichtungszeit} (bei gleicher Blende) \textbf{halbiert} 
werden.

\medskip
Jedoch \textbf{erhöht} sich das \textbf{Bildrauschen} durch die Verstärkung 
von Störsignalen.

\medskip
Je kleiner die Fläche pro Sensor-Pixel ist, je mehr nimmt das Bildrauschen 
zu.

\end{frame}
\begin{frame}{\currentname}{Beispiel}
\begin{figure}[htb] \setcounter{subfigure}{0}
	\Centerfloat
	\subfigure[Blende: 4,5 Belichtung: $\frac{1}{250}$ s ISO 25600]{%
		\includegraphics[scale=0.44]
		{Empfindlichkeit/IMG_2394.jpg}
		\label{subfig:ISO25600}
	}%
	\subfigure[Blende: 2,8 Belichtung: 0,4 s ISO 100]{%
		\includegraphics[scale=0.44]
		{Empfindlichkeit/IMG_2401.jpg}
		\label{subfig:ISO100}
	}%
	\label{fig:ISO}
\end{figure}
\begin{footnotesize}
Alle Bilder sind mit einer Canon EOS 5D Mark II mit dem Canon EF 
100mm f/2.8L Macro IS USM Aufgenommen.
\end{footnotesize}
\end{frame}
\subsubsection{Belichtungsmessung}
\begin{frame}{\currentname}{Mehrfeldmessung}
Bei der \textbf{Mehrfeldmessung}, auch \textbf{Matrixmessung} genannt wird 
das komplette Bild in \textbf{Sektoren} aufgeteilt und für jeden Sektor eine 
\textbf{eigene Messung} durchgeführt. Das so entstehende Kontrastschema lässt 
\textbf{Rückschlüsse} das \textbf{Motiv} zu. Dieses Schema wird mit 
\textbf{gespeicherten Szenarien verglichen} und so eine sinnvolle Belichtung 
erzeugt.

\medskip
Auch bei \textbf{besonders hellen/dunklen} Stellen in der \textbf{Bildmitte} 
liefert dieses verfahren \textbf{gut Ergebnisse}.

\medskip
Kann für eine Situation \textbf{kein passendes Szenario} gefunden werden wird 
das Bild meist \textbf{falsch belichtet}.
\end{frame}
\begin{frame}{\currentname}{Mittenbetonte Messung}
Die \textbf{Mittenbetonte Messung} misst nur den Bereich in der Mitte und die  
\textbf{Mittenbetonte Integral Messung} auch den Rand, gewichtet die Mitte 
aber stärker.

\bigskip
Dieses Verfahren lässt sich leicht durch \textbf{Extremwerte} in der 
\textbf{Bildmitte ablenken}. Da das Verfahren sehr einfach ist können solche 
Fehler durch eine \textbf{manuelle Belichtungskorrektur} behoben werden.
\end{frame}
\begin{frame}{\currentname}{Selektiv- \& Spotmessung}
Bei der \textbf{Selektivmessung} wird nur der Bereich um die Bildmitte 
vermessen, die \textbf{Spotmessung} nutzt einen noch kleineren Messbereich. 
Bei vielen Kameras lässt sich der zu vermessende Bereich (zusammen mit dem 
Autofokusmessfeld) verschieben.

\medskip
Beide Verfahren eignen sich gut um einzelne \textbf{Details 
herauszuarbeiten}.
\end{frame}
\subsubsection{Belichtungsprogramme}
\begin{frame}{\currentname}
Klassischerweise bietet eine Kamera mindestens \textbf{vier Belichtungsmodi} 
an:
\begin{table}[htb]
\centering
\begin{tabular}{|c|c|c|}
\hline 
\diagbox{Blende:}{Zeit:} & Manuell & Auto \\ 
\hline 
Manuell & M & Av \\ 
\hline 
Auto & Tv & P \\ 
\hline 
\end{tabular}
\end{table}
Dazu kommen meistens noch verschieden Automatikmodi die z.B. für Landschaft 
oder Portrait optimiert sind.

\medskip
Für eine gut \textbf{Bildgestaltung} bietet sich \textbf{Av} (Blendenvorwahl) 
an, da sich so die \textbf{Schärfentiefe beeinflussen} lässt. Programm 
Automatik (\textbf{P}) ist besonders für \textbf{schnelle Aufnahmen} 
geeignet, da so nicht auf die Werte geachtet werden muss. Zeitvorwahl 
(\textbf{Tv}) wird meistens in der \textbf{Sportphotographie} benutzt.
\end{frame}
\subsubsection{Autofokus}
\begin{frame}{\currentname}{Modi}
Üblicherweise stehen drei Autofokus-Modi zur Auswahl:

\medskip
\textbf{single autofocus} bei Canon One Shot genannt:\\[1pt]
Durch halb durchdrücken des Auslösers wird das Objekt im Messfeld einmal 
fokussiert und der Fokus nicht mehr geändert. Gut geeignet für unbewegliche 
Objekte und Landschaftsaufnahmen.

\medskip
\textbf{continuous autofocus}  bei Canon AI Servo genannt:\\[1pt]
Das Objekt wird fokussiert und der Fokus wird permanent nachgeführt um so 
Bewegungen auszugleichen. Gut geeignet für bewegte Objekte, wie bei Sport- 
oder Nahaufnahmen.

\medskip
\textbf{automatic autofocus} bei Canon AI Focus genannt:\\[1pt]
Die Kamera entscheidet selbst ob sich das Objekt bewegt oder nicht. So können 
statische Objekte, die anfangen sich zu bewegen fokussiert werden.
\end{frame}
\begin{frame}{\currentname}{Messfelder}
Die meisten Digitalkameras bieten mehrere AF-Messfelder an. So lassen sich 
auch Objekte die nicht in der Bildmitte sind fokusshieren, da der Photograf 
selbst bestimmt in welchem Bereich der Fokus gemessen wird. Auch hier gibt es 
meistens verschiedene Modi:

\medskip
\textbf{Einzelfeld-AF:} Es wird nur ein AF-Messfeld benutzt.

\medskip
\textbf{AF-Bereich:} Es wird ein Bereich von 5 bis 9 Messfeldern benutzt.

\medskip
\textbf{AF-Zonen:} Es werden alle Messfelder in Zonen unterteilt und der 
Photograf wählt nur noch die Zone z.B. Oben-Rechts.

\medskip
\textbf{Automatische-AF Wahl:} Die Kamera wählt die AF-Messfelder selbst aus.

\end{frame}
\subsubsection{Datei-Format}
\begin{frame}{\currentname}
Nahezu alle Digitalkameras bieten \texttt{.jpg} und/oder \texttt{.tif} als 
Dateiformat für die Bilder an. Bei einigen Modellen können die Daten auch im 
Raw-Format (z.B. \texttt{.cr2} bei Canon) gespeichert werden.

\medskip
Wenn die Kamera das \textbf{Raw-Format} anbietet sollte es auch 
\textbf{genutzt werden}, da der \textbf{Dynamikumfang} von Raw ist mit 12 Bit 
(Canon) und 12--14 Bit (Nikon) \textbf{größer} als bei von der Kamera 
generiertem \texttt{.jpg} (8 Bit) ist.

\medskip
Am Computer muss das \textbf{Raw} nach \texttt{.jpg} oder \texttt{.tif} 
\textbf{Konvertiert} werden. Dies ist zwar \textbf{aufwendiger} als direkt in 
\texttt{.jpg} aufzunehmen, bietet aber \textbf{mehr Möglichkeite}n der 
Bildgestaltung.

\end{frame}
\subsubsection{Formatfaktor}
\begin{frame}{\currentname}
Die Schärfentiefe, der Bildausschnitt (bei gleicher Brennweite) und das 
Rauschverhalten hängen von der Sensorgröße (und Auflösung) ab. 

\medskip
Es gibt eine Vielzahl von unterschiedlichen Formaten, wobei das 
\textbf{Kleinbild-Format} (KB) mit \textbf{24 mm $\times$ 36 mm} eine 
Sonderstellung einnimmt, da es ein \textbf{Standard} bei Analogen-Filmen war. 
Bei DSLRs ist heutzutage das APS-C und im oberen Preissegment das 
KB-Format üblich.

\medskip
Das \textbf{APS-C-Format} hat einen \textbf{Formatfaktor} von 1,6. Somit 
müssen alle KB-Brennweiten durch diesen Faktor \textbf{dividiert} werden, um 
den \textbf{gleichen Bildausschnitt} zu erreichen, z.B 50 mm auf einer 
KB-Kamera entsprechen 31,25 mm bei einer APS-C-Kamera: $ \frac{50\:\text{mm}}
{1,6} = 31,25\:\text{mm} $

\medskip
Weiterhin ist zu beachten, dass es \textbf{Objektive} gibt, die \textbf{nur} 
das \textbf{APS-C-Format} abdecken und nicht mit KB-Kameras funktionieren.

\end{frame}
\begin{frame}{\currentname}{Beispiel}
\begin{figure}[htb]
	\centering
	\includegraphics[scale=0.15]{Sensorformat/Sensorformate_2015_01_10.png}
	\label{fig:schaerfentiefe5}
\end{figure}
\begin{scriptsize}
Quelle: \href{https://upload.wikimedia.org/wikipedia/de/3/3d/Sensorformate_2015_01_10.svg}
{\tt upload.wikimedia.org/wikipedia/de/3/3d/Sensorformate\_2015\_01\_10.svg}
\end{scriptsize}
\end{frame}

%\subsubsection{Spiegelvorauslösung}
%\begin{frame}{\currentname}
%\end{frame}

\subsection{Blitzgeräte}

\subsubsection{Leitzahl}
\begin{frame}{\currentname}
Die \textbf{Leitzahl} ist ein \textbf{Messwert} zur 
\textbf{Leistungsklassifikation} von \textbf{Blitzgeräten}.

\medskip
Diese Leitzahl berechnet sich aus dem \textbf{Abstand} zwischen 
\textbf{Kamera und Objekt} \textbf{multipliziert} mit der \textbf{Blende}. 
Dabei wird mit ISO 100 und einer Brennweite von 50 mm gearbeitet.

\medskip
Manche Hersteller geben eine Leitzahl an, die sich auf ISO 200 bezieht. 
Dadurch erscheinen die Blitzgeräte stärker als sie real sind.

\medskip
Da moderne Blitzgeräte ihre Leistung durch digitale Messverfahren genau 
dosieren können ist die Leitzahl nur noch von \textbf{Bedeutung} für die 
\textbf{Maximale Leistung}.

\end{frame}
\subsubsection{Highspeedsynchronisation}
\begin{frame}{\currentname}
Die meisten Digitalkameras benutzen einen \textbf{Schlitzverschluss}; dabei 
gibt es zwei Vorhänge, die den Sensor verdecken. Wird ein Bild aufgenommen, 
öffnet der erste Verschluss einen Schlitz, sodass der Sensor belichtet wird. 
Nach kurzer Zeit läuft der zweite Verschluss los und verdeckt den Sensor 
wieder.

\medskip
\textbf{Nur bis} zu einer Belichtungszeit von $\frac{1}{250}$ s liegt dabei 
der \textbf{komplette Sensor frei}. Zündet ein Blitz bei einer kürzeren 
Belichtungszeit belichtet er nur noch einen Teil des Bildes.

\medskip
Um dies zu verhindern kann der Blitz in einen Highspeed-Modus (auch Focal 
Plane Blitzen genannt) geschaltet werden. Dadurch wird während der Belichtung 
\textbf{stroboskopartig} der Blitz gezündet und so der \textbf{gesamte 
Bildbereich richtig belichtet}.

\end{frame}
%\subsubsection{Mehrere Blitzgeräte}
%\begin{frame}{\currentname}
%\end{frame}
\subsubsection{Rote Augen}
\begin{frame}{\currentname}

Die \textbf{roten Augen} bei Aufnahmen mit Blitzlicht entstehen, wenn das 
Licht von der \textbf{Netzhaut} im Auge \textbf{reflektiert} wird.

\medskip
Dies geschieht, wenn der \textbf{Winkel} zwischen einstrahlendem 
\textbf{Blitzlicht} und der \textbf{Aufnahmeachse} der Kamera kleiner als 
2$^\circ$ ist.

\medskip
Besonders \textbf{Kompaktkameras} sind \textbf{anfällig} dafür, da der Blitz 
sehr nah an der Linse verbaut ist. Auch \textbf{weite Entfernungen} zwischen 
Objekt und Kamera und dunkle Räume führen zu diesem Effekt.

\medskip
Die meisten Kameras bieten eine Funktion zur \textbf{Reduzierung der roten 
Augen} an, dabei wird mit \textbf{mehreren Vorblitzen} versucht die 
\textbf{Pupillen} zu \textbf{schließen}.

\end{frame}
%\subsubsection{Synchronisation auf den zweiten Verschlussvorhang}
%\begin{frame}{\currentname}
%\end{frame}

\subsection{Zubehör}

\subsubsection{Stativ}
\begin{frame}{\currentname}

Für viele Photo-Techniken ist es notwendig die Kamera über mehrere Momente 
unbewegt an einem Platz zu belassen. Hierzu bietet sich ein Stativ an. Anders 
als ein Kameragehäuse altert ein Stativ fasst nicht.

\medskip
Üblicherweise gibt es drei mögliche Materialien aus denen ein Stativ 
gefertigt sein kann:

\begin{itemize}
	\item Aluminium: Preiswert und unempfindlich gegenüber Beschädigungen 
	aber schwer.
	\item Karbon: Teurer und empfindlich aber leicht und besonders 
	schwingungsdämpfend.
	\item Basalt: Eher selten, liegt zwischen den beiden anderen.
\end{itemize}

\end{frame}
\subsubsection{Stativ-Kopf}
\begin{frame}{\currentname}

Für verschiedene Anwendungen gibt es unterschiedliche Stativ-Köpfe, die 
wichtigsten Vertreter sollen kurz erläutert werden.

\medskip
Beim \textbf{Dreiwegeneiger} kann jede Achse an einem separaten Griff 
eingestellt werden, dabei wird jeweils nur eine Dimension eingestellt, was 
einfacher aber auch langwieriger ist.

\medskip
Ein \textbf{Kugelkopf} besteht aus einer Kugel/Fassung und einer Arretierung, 
so lassen sich mehre Achsen zeitgleich einstellen.

\medskip
Die \textbf{Getriebeneiger} sind meist sehr teuer aber extrem präzise, sie 
bieten für jede Achse ein Getriebe an, durch das die Kameraposition verstellt 
werden kann.

\medskip
Für Videoaufnahmen bietet sich ein \textbf{Fluidkopf} an, da er das fließende 
Schwenken der Kamera erlaubt.

\end{frame}

\section{Die Stilmittel}

\subsection{Digitale Bildtechniken}

\subsubsection{HDR}
\begin{frame}{\currentname}
Eine der größten \textbf{schwächen von Digitalkameras} gegenüber chemischen 
Filmen ist der \textbf{schlechtere Dynamikumfang}. Um dieses Problem zu 
umgehen bieten sich \textbf{HDR} (High Dynamic Range) Aufnahmen an.

\medskip
Dabei werden \textbf{mehrere unterschiedlich belichtete Aufnahmen} (z.B. \texttt{.cr2} 
12 Bit) digital \textbf{zu einem Bild} mit hohem Dynamikumfang zusammengefügt 
(z.B. \texttt{.hdr} 32 Bit). Diese Aufnahme wird anschließend wieder gleichmäßig 
komprimiert (z.B. \texttt{.jpg} 8 Bit) um an Monitoren oder auf Papier darstellbar zu 
sein.

\medskip
Die Ergebnisse variieren je nach Prozessparametern zwischen übersättigten, 
unrealistischen Bildern und Aufnahmen, die natürlich wirken, jedoch auch bei 
extremen Lichtsituationen noch \textbf{nicht ausgefressen} sind.

\end{frame}
\begin{frame}{\currentname ~(ctn.)}
\begin{figure}[htb] \setcounter{subfigure}{0}
	\subfigure[Unterbelichtetes Bild (-2EV)]{%
		\includegraphics[scale=0.35]
		{HDR/IMG_2131.jpg}
		\label{subfig:HDR1}
	}
	\subfigure[Richtig belichtetes Bild]{%
		\includegraphics[scale=0.35]
		{HDR/IMG_2130.jpg}
		\label{subfig:HDR2}
	}
	\subfigure[Überbelichtetes Bild (+2EV)]{%
		\includegraphics[scale=0.35]
		{HDR/IMG_2132.jpg}
		\label{subfig:HDR3}
	}
	\caption{Die Einzelteile eines HDR-Bildes}
	\label{fig:HDR}
\end{figure}
\end{frame}

\begin{frame}{\currentname ~(ctn.)}
\begin{figure}[htb]
	\centering
	\includegraphics[scale=0.8]{HDR/IMG_2130_HDR1_16x9.JPG}
	\caption{Das fertige HDR-Bild, natürliche Farben}
	\label{fig:HDRfin1}
\end{figure}
\end{frame}

\begin{frame}{\currentname ~(ctn.)}
\begin{figure}[htb]
	\centering
	\includegraphics[scale=0.8]{HDR/IMG_2130_HDR_16x9.JPG}
	\caption{Das fertige HDR-Bild, übersättigt}
	\label{fig:HDRfin2}
\end{figure}
\end{frame}

\subsubsection{Stitching}
\begin{frame}{\currentname}

Um aus einer \textbf{Reihe von teilweise überlappenden Einzelaufnahmen} ein 
\textbf{Panoramabild} zu erzeugen, können diese Aufnahmen zusammengenäht (Eng. 
to stitch) werden. So lassen sich auch $360^\circ \times 180^\circ$ Aufnahmen 
erzeugen. Über dieses Verfahren lassen sich Aufnahmen mit extrem hohen 
Auflösungen erzeugen.

\bigskip
Eine kleine Auswahl von Stitching-Software:

\begin{itemize}
	\item Adobe Photoshop
	\item Hugin
	\item PhotoStitch (Canon)
\end{itemize}

\end{frame}
\subsubsection{Focus Stacking}
\begin{frame}{\currentname}

Um in der \textbf{Makrophotographie} den extrem \textbf{kleinen 
Schärfenbereich} zu \textbf{erweitern} können mehrere Aufnahmen mit 
unterschiedlichen Fokus-Punkten übereinander gelegt (Eng. Stacking) werden um 
so den Bereich, der Scharf ist zu erweitern.

\bigskip
Eine kleine Auswahl von Focus Stacking-Software:

\begin{itemize}
	\item Helicon Focus
	\item Enfuse / MacroFusion
	\item CombineZP
\end{itemize}

\end{frame}

\subsection{Bildaufteilung und Gestaltung}

\subsubsection{Goldener Schnitt}
\begin{frame}{\currentname}

Der goldene Schnitt beschreibt ein \textbf{Teilungsverhältnis} (1:1,618\dots) 
von Strecken. Da dieses Teilungsverhältnis auch in der Natur oft vorkommt 
wird es als \textbf{besonders harmonisch} empfunden. Dabei verhält sich die 
große Strecke zur Kleinen wie die gesamte Strecke zur Großen. Dies lässt sich 
auf Flächen übertragen.

\medskip
Bilder deren Aufteilung sich an dem goldenem Schnitt orientieren werden oft 
als angenehm empfunden. Wenn man diese Gestaltungsregel anwenden möchte, 
sollte man das Hauptobjekt des Bildes an einem (gedachten) Raster das dem 
goldenem Schnitt entspricht ausrichten. Solch ein 
Raster findet sich z.B. auf Seite \autopageref{fig:GoldenRatio}.

\medskip
Dazu einige Beispiele:
\begin{itemize}
	\item \href{http://www.foto-kurs.com/bildgestaltung-goldener-schnitt.htm}
	{\tt www.foto-kurs.com/bildgestaltung-goldener-schnitt.htm}
	\item \href{http://www.dirschl.com/Bildaufbau.html}
	{\tt www.dirschl.com/Bildaufbau.html}
\end{itemize}

\end{frame}
\begin{frame}{\currentname}{Beispiel}
\begin{figure}[htb]
	\centering
	\includegraphics[scale=0.15]{Gestaltung/16:9_Goldener_Schnitt.png}
	\caption{Goldener Schnitt am Beispiel eines 16:9 Rahmens}
	\label{fig:GoldenRatio}
\end{figure}
\end{frame}
\subsubsection{Drittelregel}
\begin{frame}{\currentname}

Da es oft schwierig ist das Raster des goldenen Schnitts anzuwenden, gibt es 
eine daran angelehnte Gestaltungsregel, die das \textbf{Bild in 9 gleich 
große Felder} aufteilt. Diese Aufteilung lässt sich bei vielen Kameras im 
Sucher (oder auf dem Display) einblenden.

\medskip
z.B. bei Landschaftsaufnahmen bietet es sich an den Horizont auf eine der 
beiden horizontalen Linien zu legen. Auf Seite \pageref{fig:Drittelregel} 
findet sich ein Raster mit der Aufteilung der Drittelregel.

\end{frame}
\begin{frame}{\currentname}{Beispiel}
\begin{figure}[htb]
	\centering
	\includegraphics[scale=0.15]{Gestaltung/16:9_Drittelregel.png}
	\caption{Drittelregel am Beispiel eines 16:9 Rahmens}
	\label{fig:Drittelregel}
\end{figure}
\end{frame}
\subsubsection{Bildausschnitt}
\begin{frame}{\currentname}

\textbf{Je mehr Raum} im Bild die \textbf{Umgebung} im Verhältnis zum Objekt 
einnimmt, \textbf{je isolierter} und kleiner \textbf{wirkt das Objekt} 
selbst. So kann ein Gefühl von Größe und Raum erzeugt und das Objekt in 
Beziehung zu seiner Umwelt gesetzt werden.

\medskip
Ein \textbf{angeschnittenes Motiv}, dem ein Teil fehlt, kann dazu 
\textbf{animieren} den \textbf{Rest} des Motivs \textbf{gedanklich zu 
ergänzen}, dies kann ein gutes Stilmittel sein. Jedoch sollte dieses Mittel 
bewusst eingesetzt werden, da es sonst leicht als Flüchtigkeitsfehler 
angesehen wird. Eine Beschneidung kann meistens auch noch nachträglich am PC 
erfolgen.

\end{frame}
\subsubsection{Format}
\begin{frame}{\currentname}

Das Kleinbild-Aufnahmeformat (3:2) ist nicht das einzige Ausgabeformat. Für 
die meisten Monitore bietet sich das HD-Format (16:9) besser an. Auch das 1:1 
Format, wie es früher Mittelformat-Kameras genutzt haben kann interessant 
sein, besonders um einen ruhigen Gesamteindruck zu unterstrichen.

\medskip
Das \textbf{Querformat} ist er Klassiker, der sich gut für Beamer oder 
Bildschirm als Ausgabeformat eignet. Es entspricht dem natürlichen Sehen, da 
die Augen auch nebeneinander angeordnet sind.

\medskip
Das \textbf{Hochformat} hingegen ist unnatürlich und erzeugt so eine gewisse 
Spannung, dies bringt Dynamik ins Bild.

\end{frame}
\subsubsection{Perspektive}
\begin{frame}{\currentname}

In der \textbf{Aufsicht} wird das Objekt von oben betrachtet, wodurch es 
\textbf{kleiner und unterwürfig} erscheint, besonders bei Makroaufnahmen 
entsteht so ein ungeeigneter Eindruck, da die Objekte meist schon klein genug 
sind, in anderen Bereichen kann diese Perspektive jedoch interessant sein.

\medskip
Bei der \textbf{Normalsicht} sind Kamera und Objekt auf gleicher Höhe. 
Dadurch wirken \textbf{besonders kleine Objekte größer} und oft auch 
Interessanter.

\medskip
Die \textbf{Untersicht} stellt die Kamera unter das Objekt. So wird das Motiv 
vergrößert dargestellt, es wirkt \textbf{mächtiger}, bei machen Motiven sogar 
bedrohlich.

\end{frame}
\subsubsection{Monochrom und Farbe}
\begin{frame}{\currentname}
Monochrome Aufnahmen bieten sich immer dort an, wo die \textbf{Struktur} des 
Objektes stark betont werden soll und die Farben eine untergeordnete Rolle 
spielen.

\begin{figure}[htb] \setcounter{subfigure}{0}
	\subfigure[Mit Farbe]{%
		\includegraphics[scale=0.075]
		{SW/IMG_9929_1920_1080.jpg}
		\label{subfig:SW1}
	}
	\subfigure[Monochrom]{%
		\includegraphics[scale=0.075]
		{SW/IMG_9930_SW_1920x1080.jpg}
		\label{subfig:SW2}
	}
	\caption{Monochrom vs. Farbe In zwei aufeinanderfolgenden Aufnahmen}
	\label{fig:SW}
\end{figure}

\end{frame}
\section{Die digitale Dunkelkammer}
\subsection{Vom Raw zum fertigen Bild}
\subsubsection{Farbtemperatur}
\begin{frame}{\currentname}

Licht aus unterschiedlichen Quellen ist unterschiedlich gefärbt, das Licht 
einer Glühbirne ist roter als das der Sonne. Dieser Umstand wird als 
Farbtemperatur des Lichtes bezeichnet und in \textbf{Grad Kelvin} 
(Schwarzkörperstrahlung) gemessen. \textbf{Je höher der Wert je blauer das 
Licht}.

\begin{figure}[htb]
	\centering
	\includegraphics[scale=0.15]{Farbtemperatur/2000px-Color_temperature_sRGB.png}
	\label{fig:schaerfentiefe3}
\end{figure}

\begin{tiny}
Quelle: \href{https://upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Color_temperature_sRGB.svg/2000px-Color_temperature_sRGB.svg.png}
{\tt upload.wikimedia.org/wikipedia/commons/thumb/5/5a/Color\_temperature\_sRGB.svg/2000px-Color\_temperature\_sRGB.svg.png}
\end{tiny}

\end{frame}
\subsubsection{Weißabgleich}
\begin{frame}{\currentname}

Das menschliche \textbf{Auge passt} sich der \textbf{jeweiligen 
Farbtemperatur an}, so das weiß immer als weiß erscheint. Eine 
\textbf{Kamera} kann dies \textbf{nur bedingt} automatisch, deswegen ist 
meist eine manuelle Korrektur nötig; der Weißabgleich.

\medskip
Dazu bieten viele Raw-Entwickler das Pipetten-Werkzeug und eine direkte 
Einstellung der Grad Kelvin an. Für das Pipetten-Werkzeug sucht man sich eine 
Fläche im Bild, die weiß oder neutral-grau sein sollte und nimmt von dort die 
Farbe auf.

\end{frame}
\subsubsection{Objektivkorrektur}
\begin{frame}{\currentname}

Auch teure und moderne objektive sind nicht frei von 
\textbf{Abbildungsfehlern} doch einige dieser Fehler lassen sich 
\textbf{nachträglich} am PC leicht \textbf{korrigieren}. Dazu sind meist 
\textbf{speziell} für das benutzte Objektiv erstelle \textbf{Datensätze 
nötig}, die jedoch i.d.R. frei verfügbar sind.

\medskip
Die \textbf{Verzeichnungskorrektur} sollte nur durchgeführt werden, wenn sie 
notwendig sind, da sie die Bildqualität (Schärfe) verschlechtern. Eine 
Korrektur der \textbf{Vignettierung} verschlechtert die Bildqualität nicht.

\end{frame}
\subsubsection{Gradationskurve \& Histogramm}
\begin{frame}{\currentname}

Das \textbf{Histogramm} zeigt die \textbf{Häufigkeit eines Farbwertes pro 
Pixel} an. Dabei sind üblicherweise die \textbf{dunklen Werte links} und die 
hellen rechts aufgetragen. An einem Histogramm lassen sich viele Einstellung 
gut Visualisieren z.B. der \textbf{Schwarzwert}.

\medskip
Die \textbf{Gradationskurve} definiert das \textbf{Eingabe/Ausgabe 
Verhältnis} eines Farbwertes. Beginnend mit einer neutralen ($x=y$) Kurve lässt 
sich so die Helligkeit von \textbf{helle und dunkel Bereiche getrennt} von 
einander einstellen (Kontrast).

\end{frame}
\begin{frame}{Quellen}

\QuoteM{Digitale Fotopraxis Makrofotografie} von Björn K. Langlotz, ISBN 978-3-8362-1663-0

\bigskip
\QuoteM{Heute schon geblitzt ?} von Dirk Wächter, ISBN 978-3-00-02477-9

\bigskip
DPP Tutorial: \href{https://www.youtube.com/watch?v=8s6yMkhHJkE}
{\tt www.youtube.com/watch?v=8s6yMkhHJkE}

\end{frame}
\begin{frame}
\Huge \center Noch Fragen ?
\end{frame}
\end{document}