Lecture script from the [Summer School 2016](https://www.fh-bielefeld.de/minden/summer-school/module/einfuehrung-in-photokameratechnik)
Photo course.

LICENSE of the LaTeX source is: [Attribution-NonCommercial-ShareAlike 4.0 International](https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode)
